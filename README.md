# yocto



## Getting started

install superflore from : https://github.com/ros-infrastructure/superflore#installation
and pocky by doing :
```
     $ git clone http://git.yoctoproject.org/git/poky
     $ cd poky
     $ git checkout -b fido origin/fido
     $ source oe-init-build-env
```
